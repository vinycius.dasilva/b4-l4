package com.jala.university.sd3.b4l4.sales.entities;

import com.jala.university.sd3.b4l4.shared.entities.User;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.jpa.domain.AbstractAuditable;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Table(name = "TB_SALES")
public class Sale extends AbstractAuditable<User, UUID> {
    private UUID productUUID;
    @Column(name = "NM_QUANTITY")
    private BigDecimal quantity;
    @Temporal(TemporalType.DATE)
    @Column(name = "DT_SELL")
    private Date soldDate;

    @PrePersist
    private void prePersist() {
        setSoldDate(new Date());
    }
}
