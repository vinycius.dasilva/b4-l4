package com.jala.university.sd3.b4l4.products.factory;

import com.jala.university.sd3.b4l4.products.dto.ProductRequestDto;
import com.jala.university.sd3.b4l4.products.dto.ProductResponseDto;
import com.jala.university.sd3.b4l4.products.entities.Product;

public class ProductFactory {
    public static ProductResponseDto createProductResponseDto(Product product) {
        return new ProductResponseDto(product.getId(), product.getName(), product.getPrice(), product.getStock());
    }

    public static Product createProduct(ProductRequestDto productRequestDto) {
        return new Product(productRequestDto.name(), productRequestDto.price(), productRequestDto.stock());
    }
}
