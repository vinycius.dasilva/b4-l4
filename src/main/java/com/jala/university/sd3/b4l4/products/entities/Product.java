package com.jala.university.sd3.b4l4.products.entities;

import com.jala.university.sd3.b4l4.shared.entities.User;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;
import org.springframework.data.jpa.domain.AbstractAuditable;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Table(name = "TB_PRODUCTS")
public class Product extends AbstractAuditable<User, UUID> {
    @Column(name = "NM_PRODUCT")
    private String name;
    @Column(name = "VL_PRICE", precision = 10, scale = 2)
    private BigDecimal price;
    @Column(name = "NM_STOCK", precision = 10, scale = 2)
    private BigDecimal stock;
}
