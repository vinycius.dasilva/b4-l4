package com.jala.university.sd3.b4l4.products.dto;

import jakarta.validation.constraints.Positive;
import lombok.NonNull;

import java.math.BigDecimal;

public record ProductRequestDto(@NonNull String name, @Positive BigDecimal price, @Positive BigDecimal stock) {
}
