package com.jala.university.sd3.b4l4.products.controllers;

import com.jala.university.sd3.b4l4.products.dto.ProductRequestDto;
import com.jala.university.sd3.b4l4.products.dto.ProductResponseDto;
import com.jala.university.sd3.b4l4.products.services.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductResponseDto>> getProducts() {
        return ResponseEntity.ok(productService.getProducts());
    }

    @GetMapping(path = "{productID}")
    public ResponseEntity<ProductResponseDto> getProduct(@PathVariable("productID") UUID uuid) {
        return ResponseEntity.ok(productService.getProduct(uuid));
    }

    @PostMapping
    public ResponseEntity<ProductResponseDto> createProduct(@RequestBody @Valid ProductRequestDto productRequestDto) {
        return ResponseEntity.ok(productService.createProduct(productRequestDto));
    }

    @DeleteMapping(path = "{productID}")
    public void deleteProduct(@PathVariable("productID") UUID uuid) {
        productService.deleteProduct(uuid);
    }

    @PutMapping(path = "{productID}")
    public ResponseEntity<ProductResponseDto> updateProduct(@PathVariable("productID") UUID uuid, @RequestBody @Valid ProductRequestDto productRequestDto) {
        return ResponseEntity.ok(productService.updateProduct(uuid, productRequestDto));
    }
}
