package com.jala.university.sd3.b4l4.products.repositories;

import com.jala.university.sd3.b4l4.products.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {
}
