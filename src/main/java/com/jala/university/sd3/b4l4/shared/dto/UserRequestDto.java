package com.jala.university.sd3.b4l4.shared.dto;

import jakarta.validation.constraints.NotNull;

public record UserRequestDto(@NotNull String name) {
}
