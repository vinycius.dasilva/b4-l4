package com.jala.university.sd3.b4l4.sales.repositories;

import com.jala.university.sd3.b4l4.sales.entities.Sale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SaleRepository extends JpaRepository<Sale, UUID> {
}
