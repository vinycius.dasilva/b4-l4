package com.jala.university.sd3.b4l4.shared.entities;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Table(name = "TB_USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "ID", updatable = false)
    private UUID uuid;
    @Setter
    @Column(name = "NM_USER", updatable = false)
    private String name;
}
