package com.jala.university.sd3.b4l4.sales.services;

import com.jala.university.sd3.b4l4.products.dto.ProductRequestDto;
import com.jala.university.sd3.b4l4.products.repositories.ProductRepository;
import com.jala.university.sd3.b4l4.products.services.ProductService;
import com.jala.university.sd3.b4l4.sales.dto.SaleRequestDto;
import com.jala.university.sd3.b4l4.sales.dto.SaleResponseDto;
import com.jala.university.sd3.b4l4.sales.dto.SaleResponseDtoWithTotal;
import com.jala.university.sd3.b4l4.sales.entities.Sale;
import com.jala.university.sd3.b4l4.sales.factory.SaleFactory;
import com.jala.university.sd3.b4l4.sales.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SaleService {
    @Autowired
    private SaleRepository saleRepository;
    @Autowired
    private ProductService productService;

    public List<SaleResponseDto> getSales() {
        return saleRepository.findAll().stream().map(SaleFactory::createSaleResponseDto).collect(Collectors.toList());
    }

    public SaleResponseDtoWithTotal createSale(SaleRequestDto saleRequestDto) {
        var sale = SaleFactory.createSale(saleRequestDto);
        if (isPossibleToSell(sale)) {
            var newSold = saleRepository.save(sale);
            reduceProductQuantity(sale);
            return SaleFactory.createSaleResponseDtoWithTotal(newSold, calculateTotalPrice(newSold));
        }
        return null;
    }

    private boolean isPossibleToSell(Sale sale) {
        var saleQuantity = sale.getQuantity();
        var product = productService.getProduct(sale.getProductUUID());

        if (saleQuantity.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Quantity must be a positive number.");
        }

        return product.stock().compareTo(saleQuantity) >= 0;
    }

    private void reduceProductQuantity(Sale sale) {
        var product = productService.getProduct(sale.getProductUUID());
        var productRequest = new ProductRequestDto(product.name(), product.price(), product.stock().subtract(sale.getQuantity()));
        productService.updateProduct(sale.getProductUUID(), productRequest);
    }

    private BigDecimal calculateTotalPrice(Sale sale) {
        var product = productService.getProduct(sale.getProductUUID());
        var newPrice = product.price().multiply(BigDecimal.valueOf(saleDiscount(sale.getQuantity())));
        return sale.getQuantity().multiply(newPrice);
    }

    private float saleDiscount(BigDecimal quantity) {
        if (quantity.longValue() >= 20) return 0.90f;
        else if (quantity.longValue() >= 10) {
            return 0.95f;
        }
        return 1;
    }
}
