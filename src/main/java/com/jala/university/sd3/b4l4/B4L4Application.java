package com.jala.university.sd3.b4l4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class B4L4Application {

    public static void main(String[] args) {
        SpringApplication.run(B4L4Application.class, args);
    }

}
