package com.jala.university.sd3.b4l4.sales.controllers;

import com.jala.university.sd3.b4l4.sales.dto.SaleRequestDto;
import com.jala.university.sd3.b4l4.sales.dto.SaleResponseDto;
import com.jala.university.sd3.b4l4.sales.dto.SaleResponseDtoWithTotal;
import com.jala.university.sd3.b4l4.sales.services.SaleService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("api/v1/sale")
public class SaleController {
    @Autowired
    private SaleService saleService;

    @GetMapping
    public ResponseEntity<List<SaleResponseDto>> getSales() {
        return ResponseEntity.ok(saleService.getSales());
    }

    @PostMapping
    public ResponseEntity<SaleResponseDtoWithTotal> createSale(@RequestBody @Valid SaleRequestDto saleRequestDto) {
        var sale = saleService.createSale(saleRequestDto);
        if (Objects.isNull(sale))
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        return ResponseEntity.ok(sale);
    }
}
