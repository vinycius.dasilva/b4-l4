package com.jala.university.sd3.b4l4.products.services;

import com.jala.university.sd3.b4l4.products.dto.ProductRequestDto;
import com.jala.university.sd3.b4l4.products.dto.ProductResponseDto;
import com.jala.university.sd3.b4l4.products.factory.ProductFactory;
import com.jala.university.sd3.b4l4.products.repositories.ProductRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<ProductResponseDto> getProducts() {
        return productRepository.findAll().stream().map(ProductFactory::createProductResponseDto).collect(Collectors.toList());
    }

    public ProductResponseDto getProduct(UUID uuid) {
        var product = productRepository.findById(uuid);
        return product.map(ProductFactory::createProductResponseDto).orElse(null);
    }

    public ProductResponseDto createProduct(ProductRequestDto productRequestDto) {
        var product = ProductFactory.createProduct(productRequestDto);
        var createdProduct = productRepository.save(product);
        return ProductFactory.createProductResponseDto(createdProduct);
    }

    public void deleteProduct(UUID uuid) {
        var product = productRepository.findById(uuid);
        product.ifPresent(value -> productRepository.deleteById(Objects.requireNonNull(value.getId())));
    }

    @Transactional
    public ProductResponseDto updateProduct(UUID uuid, ProductRequestDto productRequestDto) {
        var product = productRepository.findById(uuid).orElseThrow(() -> new IllegalStateException("This product ID " + uuid + " doesn't exists!"));

        if (!Objects.equals(product.getName(), productRequestDto.name())) product.setName(productRequestDto.name());
        if (!Objects.equals(product.getPrice(), productRequestDto.price())) product.setPrice(productRequestDto.price());
        if (!Objects.equals(product.getStock(), productRequestDto.stock())) product.setStock(productRequestDto.stock());

        return ProductFactory.createProductResponseDto(product);
    }
}
