package com.jala.university.sd3.b4l4.sales.factory;

import com.jala.university.sd3.b4l4.sales.dto.SaleRequestDto;
import com.jala.university.sd3.b4l4.sales.dto.SaleResponseDto;
import com.jala.university.sd3.b4l4.sales.dto.SaleResponseDtoWithTotal;
import com.jala.university.sd3.b4l4.sales.entities.Sale;

import java.math.BigDecimal;
import java.sql.Statement;
import java.util.Date;
import java.util.UUID;

public class SaleFactory {
    public static SaleResponseDto createSaleResponseDto(Sale product) {
        return new SaleResponseDto(product.getProductUUID(), product.getQuantity(), product.getSoldDate());
    }

    public static Sale createSale(SaleRequestDto productRequestDto) {
        return new Sale(UUID.fromString(productRequestDto.productUUID()), productRequestDto.quantity(), new Date());
    }

    public static SaleResponseDtoWithTotal createSaleResponseDtoWithTotal(Sale sale, BigDecimal totalPrice) {
        return new SaleResponseDtoWithTotal(sale.getId(), sale.getProductUUID(), sale.getQuantity(), sale.getSoldDate(), totalPrice);
    }
}
