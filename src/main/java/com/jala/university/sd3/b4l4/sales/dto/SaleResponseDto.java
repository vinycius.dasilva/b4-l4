package com.jala.university.sd3.b4l4.sales.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import jakarta.validation.constraints.Positive;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

public record SaleResponseDto(@NotNull UUID productUUID, @Positive BigDecimal quantity, @PastOrPresent Date sold) {
}
