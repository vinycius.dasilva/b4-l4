package com.jala.university.sd3.b4l4.shared.controllers;

import com.jala.university.sd3.b4l4.shared.dto.UserRequestDto;
import com.jala.university.sd3.b4l4.shared.dto.UserResponseDto;
import com.jala.university.sd3.b4l4.shared.entities.User;
import com.jala.university.sd3.b4l4.shared.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @PostMapping
    public ResponseEntity<UserResponseDto> createUser(@RequestBody UserRequestDto user) {
        var userConverted = User.builder().name(user.name()).build();
        var userSaved = userRepository.save(userConverted);
        return ResponseEntity.ok(new UserResponseDto(userSaved.getName()));
    }
}
