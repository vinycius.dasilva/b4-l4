package com.jala.university.sd3.b4l4.shared.repositories;

import com.jala.university.sd3.b4l4.shared.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
}
