package com.jala.university.sd3.b4l4.products.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.UUID;

public record ProductResponseDto(@NotNull UUID uuid, @NonNull String name, @Positive BigDecimal price, @Positive BigDecimal stock) {
}
