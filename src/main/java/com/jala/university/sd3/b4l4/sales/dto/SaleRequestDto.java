package com.jala.university.sd3.b4l4.sales.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import java.math.BigDecimal;

public record SaleRequestDto(@NotNull String productUUID, @Positive BigDecimal quantity) {
}
